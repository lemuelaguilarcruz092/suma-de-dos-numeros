<!DOCTYPE html>
<html>
<head>
    <title>Suma de dos números</title>
</head>
<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero1 = $_POST['numero1'];
        $numero2 = $_POST['numero2'];
        $suma = $numero1 + $numero2;
        echo "La suma de " . $numero1 . " y " . $numero2 . " es: " . $suma;
    }
    ?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="numero1">Número 1:</label>
        <input type="number" id="numero1" name="numero1">
        <label for="numero2">Número 2:</label>
        <input type="number" id="numero2" name="numero2">
        <input type="submit" value="Calcular">
    </form>
</body>
</html>
